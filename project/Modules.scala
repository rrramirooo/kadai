import sbt._
import sbt.Keys._
import Settings._
import Dependencies._

trait Modules {
  lazy val core = 
    module("core")

  lazy val result = 
    module("result").dependsOn(core)

  lazy val cmdopts =
    module("cmdopts").dependsOn(core)

  lazy val config = 
    module("config").dependsOn(core, result)

  lazy val concurrent = 
    module("concurrent").dependsOn(config)

  lazy val hash = 
    module("hash").dependsOn(core)

  lazy val all = 
    module(
      id = "all"
    , fileName = Some(".")
    ) dependsOn ( // needs both dependsOn and aggregate to produce dependencies in the pom
      core, result, config, cmdopts, concurrent, hash
    ) aggregate (
      core, result, config, cmdopts, concurrent, hash
    )

  def module(id: String, fileName: Option[String] = None) = 
    Project(
      id = id
    , base = file(fileName.getOrElse(id))
    , settings = standardSettings
    ).settings(
      name := s"kadai-$id"
    , libraryDependencies ++= commonTest
    )
}
